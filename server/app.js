/*
Server side js
 */

"use strict";
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.PORT || 3000;

app.use(express.static(__dirname + "/../client/"));

app.use((req,res)=>{
    var x = 6;
    try{
        res.send("<h1>Oops, wrong page</h1>");
    }catch(error){
        console.log(error);
        res.status(500).send("ERROR");
    } 
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app;

app.get('api/register',(req, res)=>{
    console.log("Get registrations");
    res.status(200);
})

