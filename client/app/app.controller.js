(function () {
    'use strict';
    angular.module("RegApp").controller("RegCtrl", RegCtrl);
    
    RegCtrl.$inject = ["RegServiceAPI", "$log"];

    function RegCtrl(RegServiceAPI, $scope, $http, $log) {
        var regCtrlself  = this;
        
        regCtrlself.onSubmit = onSubmit;
        regCtrlself.onReset = onReset; 

        regCtrlself.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        regCtrlself.user = {
            
        }

        function onSubmit(){
            console.log(RegServiceAPI); 
            RegServiceAPI.register(RegServiceAPI, ["RegServiceApi", "$log"])
                .then((result)=>{
                    regCtrlself.user = result.data;
                }).catch((error)=>{
                    console.log(error);
                });

                $scope.onSubmit = RegServiceAPI;
                $scope.RegService = function() {
        
                $http({
                    method : 'POST',
                    url : '/submit',
                    data : $scope.user
                });
            } 
                location = '/submit.html'; 
        }

        function onReset(){
            regCtrlself.user = Object.assign({}, regCtrlself.user);
            regCtrlself.registrationform.$setPristine();
            regCtrlself.registrationform.$setUntouched();
        }
        
    }
})();